<?php
namespace App\Birthday;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO;

class Birthday extends DB{

    public $id;
    public $name="user";
    public $birthday="";

    public function __construct()
    {

        parent::__construct();
    }
    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id =$data['id'];
        }
        if(array_key_exists('name',$data)){


            $this->name= $data['name'];

        }
        if(array_key_exists('birthday',$data)){

            $this->birthday = $data['birthday'];

        }

    }
    public function store(){
        $arrData = array($this->name,$this->birthday);
        $sql = "INSERT INTO birthday(name,birthday) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::message("Data has been inserted successfully! :)");
        else
            Message::message("Your Data does not inserted. :(");

        Utility::redirect('create.php');

    }
    public function index(){


    }

}